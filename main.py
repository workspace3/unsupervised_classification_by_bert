#! -*- coding:utf-8 -*-
# 美肤助手分类，利用MLM做 Zero-Shot/Few-Shot/Semi-Supervised Learning
# 在https://github.com/bojone/Pattern-Exploiting-Training基础上做了符合自己数据的改动

import os
import numpy as np
from bert4keras.backend import keras, K
from bert4keras.layers import Loss
from bert4keras.tokenizers import Tokenizer
from bert4keras.models import build_transformer_model
from bert4keras.optimizers import Adam
from bert4keras.snippets import sequence_padding, DataGenerator
# from bert4keras.snippets import open
from keras.layers import Lambda, Dense

os.environ["CUDA_VISIBLE_DEVICES"] = ""

label_mapping = {
    u"__label__询问产品": u"产品",
    u"__label__护肤步骤": u"步骤",
    u"__label__肌肤问题": u"肌肤",
    u"__label__询问安全": u"安全",
    # "__label__询问包含某成分的产品": "成分产品",
    u"__label__询问产品的成分": u"成分",
    u"__label__询问功效": u"功效",
    u"__label__询问口碑": u"口碑",
    # u"__label__询问状态（拍照测肤）": u"状态"
}

# labels = [
#     u'文化', u'娱乐', u'体育', u'财经', u'房产', u'汽车', u'教育', u'科技', u'军事', u'旅游', u'国际',
#     u'证券', u'农业', u'电竞', u'民生'
# ]
labels = [
    u'化妆', u'护肤', u'美发', u'美甲', u'口腔', u'行业', u'成分', u'原料',
    u'人体', u'工具', u'病症', u'病源', u'食物', u'功效', u'时期', u'特性',
    u'其他',
]
# labels = list(label_mapping.values())
num_classes = len(labels)
maxlen = 32
batch_size = 64
config_path = '../bert/chinese_L-12_H-768_A-12/bert_config.json'
checkpoint_path = '../bert/chinese_L-12_H-768_A-12/bert_model.ckpt'
dict_path = '../bert/chinese_L-12_H-768_A-12/vocab.txt'
domain_pretrained_model_path = './domain_pretrained_model.weights'


def load_data(filename):
    D = []
    with open(filename, encoding='utf-8') as fi:
        for line in fi.readlines():
            parts = line.strip().split("\t")
            if len(parts) != 2:
                continue
            label, text = parts
            # label = label_mapping.get(label, None)
            # if label is None:
            #     continue
            D.append((text, label))
    return D


# 加载数据集，只截取一部分，模拟小数据集
train_data = load_data('./data/entity_classification/train.tsv')
# valid_data = load_data('./data/entity_classification/dev.tsv')
# test_data = load_data('./data/beauty/test.tsv')

# 模拟标注和非标注数据
# 标注数据的比例
# train_frac = 0.02
# num_labeled = int(len(train_data) * train_frac)
# 标注数据数量，固定值
num_labeled = 49
unlabeled_data = [(t, u'无标签') for t, l in train_data[num_labeled:]]
train_data = train_data[:num_labeled]
valid_data = train_data[0:]
train_data = train_data + unlabeled_data

# 建立分词器
tokenizer = Tokenizer(dict_path, do_lower_case=True)

# 对应的任务描述
prefix = u'对于化妆，其中包含了以下要素:'
mask_idxs = [3, 4]


def random_masking(token_ids):
    """对输入进行随机mask
    """
    rands = np.random.random(len(token_ids))
    source, target = [], []
    for r, t in zip(rands, token_ids):
        if r < 0.15 * 0.8:
            source.append(tokenizer._token_mask_id)
            target.append(t)
        elif r < 0.15 * 0.9:
            source.append(t)
            target.append(t)
        elif r < 0.15:
            source.append(np.random.choice(tokenizer._vocab_size - 1) + 1)
            target.append(t)
        else:
            source.append(t)
            target.append(0)
    return source, target


class data_generator(DataGenerator):
    """数据生成器
    """
    def __iter__(self, random=False):
        batch_token_ids, batch_segment_ids, batch_output_ids = [], [], []
        for is_end, (text, label) in self.sample(random):
            if len(label) == 2:
                text = prefix + text
            token_ids, segment_ids = tokenizer.encode(text, maxlen=maxlen)
            if random:
                source_ids, target_ids = random_masking(token_ids)
            else:
                source_ids, target_ids = token_ids[:], token_ids[:]
            if len(label) == 2:
                label_ids = tokenizer.encode(label)[0][1:-1]
                for i, j in zip(mask_idxs, label_ids):
                    source_ids[i] = tokenizer._token_mask_id
                    target_ids[i] = j
            batch_token_ids.append(source_ids)
            batch_segment_ids.append(segment_ids)
            batch_output_ids.append(target_ids)
            if len(batch_token_ids) == self.batch_size or is_end:
                batch_token_ids = sequence_padding(batch_token_ids)
                batch_segment_ids = sequence_padding(batch_segment_ids)
                batch_output_ids = sequence_padding(batch_output_ids)
                yield [
                    batch_token_ids, batch_segment_ids, batch_output_ids
                ], None
                batch_token_ids, batch_segment_ids, batch_output_ids = [], [], []


class CrossEntropy(Loss):
    """交叉熵作为loss，并mask掉输入部分
    """
    def compute_loss(self, inputs, mask=None):
        y_true, y_pred = inputs
        y_mask = K.cast(K.not_equal(y_true, 0), K.floatx())
        accuracy = keras.metrics.sparse_categorical_accuracy(y_true, y_pred)
        accuracy = K.sum(accuracy * y_mask) / K.sum(y_mask)
        self.add_metric(accuracy, name='accuracy')
        loss = K.sparse_categorical_crossentropy(y_true, y_pred)
        loss = K.sum(loss * y_mask) / K.sum(y_mask)
        return loss


# 加载预训练模型
model = build_transformer_model(
    config_path=config_path, checkpoint_path=checkpoint_path, with_mlm=True
)
# 如果有领域预训练模型，那么直接加载领域预训练模型的参数
if os.path.exists(domain_pretrained_model_path):
    model.load_weights(domain_pretrained_model_path)

# 训练用模型
y_in = keras.layers.Input(shape=(None,))
outputs = CrossEntropy(1)([y_in, model.output])

train_model = keras.models.Model(model.inputs + [y_in], outputs)
train_model.compile(optimizer=Adam(1e-5))
train_model.summary()

# 转换数据集
train_generator = data_generator(train_data, batch_size)
valid_generator = data_generator(valid_data, batch_size)
# test_generator = data_generator(test_data, batch_size)


class Evaluator(keras.callbacks.Callback):
    def __init__(self):
        self.best_val_acc = 0.

    def on_epoch_end(self, epoch, logs=None):
        model.save_weights('mlm_model.weights')
        val_acc = evaluate(valid_generator)
        if val_acc > self.best_val_acc:
            self.best_val_acc = val_acc
            model.save_weights('best_model.weights')
        # test_acc = evaluate(test_generator)
        print(
            u'val_acc: %.5f, best_val_acc: %.5f\n' %
            (val_acc, self.best_val_acc)
        )


def evaluate(data):
    label_ids = np.array([tokenizer.encode(l)[0][1:-1] for l in labels])
    total, right = 0., 0.
    for x_true, _ in data:
        x_true, y_true = x_true[:2], x_true[2]
        y_pred = model.predict(x_true)[:, mask_idxs]
        y_pred = y_pred[:, 0, label_ids[:, 0]] * \
                 y_pred[:, 1, label_ids[:, 1]]
                 # y_pred[:, 2, label_ids[:, 2]] * \
                 # y_pred[:, 3, label_ids[:, 3]]
        y_pred = y_pred.argmax(axis=1)
        y_true = np.array([
            labels.index(tokenizer.decode(y)) for y in y_true[:, mask_idxs]
        ])
        total += len(y_true)
        right += (y_true == y_pred).sum()
    return right / total


if __name__ == '__main__':
    from tqdm import tqdm

    def predict(t):
        label_ids = np.array([tokenizer.encode(l)[0][1:-1] for l in labels])
        _data = [(t, u'待定')]
        _generator = data_generator(_data, 1)
        embedded, _ = list(_generator)[0]
        inputs = embedded[0:2]
        y_pred = model.predict(inputs)[:, mask_idxs]
        y_pred = y_pred[:, 0, label_ids[:, 0]] * \
                 y_pred[:, 1, label_ids[:, 1]]
                 # y_pred[:, 2, label_ids[:, 2]] * \
                 # y_pred[:, 3, label_ids[:, 3]]
        y_pred = y_pred.argmax(axis=1)[0]
        label = labels[y_pred]
        return label
    #

    is_training = True
    if is_training:
        evaluator = Evaluator()

        train_model.fit_generator(
            train_generator.forfit(),
            steps_per_epoch=len(train_generator),
            # steps_per_epoch=10,
            epochs=5,
            callbacks=[evaluator]
        )
    else:
        model.load_weights('best_model.weights')
        # t = input("请输入: ")
        # label = predict(t)
        # print("label: {}".format(label))
        result = []
        for t, _ in tqdm(unlabeled_data):
            _label = predict(t)
            result.append((t, _label))
        with open("result.txt", "w", encoding="utf8")as fo:
            fo.write("\n".join(map("\t".join, result)) + "\n")
        print("done.")
