# coding: utf8
import os

import torch

os.environ["CUDA_VISIBLE_DEVICES"] = ""

import sys
import json
import jieba
from tqdm import tqdm
from datetime import datetime
from transformers import BertTokenizer, AlbertForPreTraining, AlbertConfig, BertForPreTraining, BertConfig
from torch.nn import Softmax
from torch.utils.data import Dataset, DataLoader
from torch.nn import CrossEntropyLoss
from torch.optim import Adam

# 实体类型定义
# labels = [
#     u'化妆', u'护肤', u'美发', u'美甲', u'口腔', u'行业', u'成分', u'原料',
#     u'人体', u'工具', u'病症', u'病源', u'食物', u'功效', u'时期', u'特性',
#     # u'其他',
# ]
labels = [
    u'病虫害', u'农作物', u'地区'
]

# 预训练模型加载
pretrained_model_path = "../albert_base"
config = AlbertConfig.from_pretrained(pretrained_model_path+"/config.json")
tokenizer = BertTokenizer.from_pretrained(pretrained_model_path+"/vocab.txt")
model = AlbertForPreTraining.from_pretrained(pretrained_model_path+"/pytorch_model.bin", config=config)
# pretrained_model_path = "../bert_base"
# config = BertConfig.from_pretrained(pretrained_model_path+"/config.json")
# tokenizer = BertTokenizer.from_pretrained(pretrained_model_path+"/vocab.txt")
# model = BertForPreTraining.from_pretrained(pretrained_model_path+"/pytorch_model.bin", config=config)
softmax = Softmax(dim=-1)

# prompt 定义
prompt = ["[原始句子]", tokenizer.sep_token, "[实体]", "属于", "[实体类型]", "的一种。"]
# prompt = ["常见的", "[实体类型]", "有", "[实体]", "等。", "[原始句子]"]
sentence_index, entity_index, entity_type_index = 0, 2, 4

# 全局参数定义
max_length = 256
epochs = 3
batch_size = 8
lr = 2e-5


def add_prompt(sent, entity, entity_type):
    prompt_copy = prompt[0:]
    prompt_copy[sentence_index] = sent[0:]
    prompt_copy[entity_index] = entity
    prompt_copy[entity_type_index] = entity_type
    sentence = "".join(prompt_copy)
    return sentence


class TrainDataset(Dataset):
    def __init__(self, train_sentences):
        super(TrainDataset, self).__init__()
        self.data = train_sentences

    def __getitem__(self, item):
        return self.data[item]

    def __len__(self):
        return len(self.data)


def masked_cross_entropy(y_pred, y_true, mask):
    cross_entropy_loss = CrossEntropyLoss(reduction="none")
    mask = torch.flatten(mask)
    y_pred = torch.flatten(y_pred, start_dim=0, end_dim=1)
    y_true = torch.flatten(y_true)
    loss = cross_entropy_loss(y_pred, y_true)
    loss = torch.sum(loss * mask) / torch.sum(mask)
    return loss


def train():
    # 加载训练数据
    f = "data/agriculture/pests.jsonl"
    train_data = []
    with open(f, encoding="utf8")as fi:
        data = fi.read().strip().split("\n")
        for item in data:
            item = json.loads(item)
            labels = item["label"]
            sent = item["data"]
            if len(labels) > 0:
                for s, e, _type in labels:
                    entity = sent[s:e]
                    new_sent = add_prompt(sent, entity, _type)
                    train_data.append(new_sent)
            else:
                train_data.append(sent)
    # 训练流程
    optimizer = Adam(model.parameters(), lr=lr)
    cross_entropy_loss = CrossEntropyLoss()
    data_loader = DataLoader(TrainDataset(train_data), batch_size=batch_size)
    for epoch in range(epochs):
        print("{}, training on epoch: {}/{}".format(datetime.now(), epoch+1, epochs))
        sys.stdout.flush()
        model.train()
        avg_loss = []
        bar = tqdm(data_loader)
        for train_batch in bar:
            # 梯度清零
            optimizer.zero_grad()
            #
            inputs = tokenizer(train_batch,
                               padding=True, max_length=max_length,
                               truncation=True, return_tensors="pt")
            outputs = model(**inputs)
            logits = outputs[0]
            mask = torch.where(inputs["input_ids"] == 0, 0, 1)
            # loss = cross_entropy_loss(logits[:, 1:-1, :].reshape((-1, logits.shape[-1])),
            #                           inputs["input_ids"][:, 1:-1].reshape((-1,)))
            loss = masked_cross_entropy(logits[:, 1:-1, :], inputs["input_ids"][:, 1:-1], mask[:, 1:-1])
            loss.backward()
            #
            optimizer.step()
            #
            bar.set_postfix({"training loss": round(loss.item(), 7)})
            # print("\t{}, training loss: {:.5f}".format(datetime.now(), loss.item()))
            sys.stdout.flush()
            avg_loss.append(loss.item())
        #
        print("\t{}, avg loss: {:.5f}".format(datetime.now(), sum(avg_loss)/len(avg_loss)))
        sys.stdout.flush()
    # 保存模型
    torch.save(model.state_dict(), "best_model.pt")
    print("{}, training is done.".format(datetime.now()))


def load_data():
    """
    加载数据
    :return:
    """
    max_length = 512
    f = "./data/agriculture/test.txt"
    with open(f, encoding="utf8")as fi:
        data = fi.read()
        data = data.split("\n")
        output = []
        for d in data:
            if len(d) <= max_length:
                output.append(d)
            else:
                output.extend([part for part in d.split("。") if len(part) <= max_length])
        #
        output = ["".join(s.split()) for s in output if len("".join(s.split())) > 0]
    print("获取{}条文本数据.".format(len(output)))
    print("文本示例（前10条）：\n\t{}".format("\n\t".join(output[0:10])))
    return output


def gen_candidates(s, max_ngram=5):
    """
    输入句子，生成备选实体列表
    :param s:
    :param max_ngram: 最大ngram值
    :return:
    """

    def is_all_chinese(strs):
        for _char in strs:
            if not '\u4e00' <= _char <= '\u9fa5':
                return False
        return True

    # tokens = tokenizer.tokenize(s)
    tokens = [word for word in jieba.cut(s)]
    # print(tokens)
    entities = ["".join(tokens[i: i+ngram]) for ngram in range(1, max_ngram+1) for i in range(len(tokens)-ngram+1)]
    entities = list(set(entities))
    entities = [e for e in entities if is_all_chinese(e)]
    return entities


# s = "测试啊apple测试"*10
# print(len(gen_candidates(s)))


def predict(s, n_best=5):
    """
    提取句子中的实体
    :param s:
    :param n_best:
    :return:
    """
    # 生成备选实体
    candidates = gen_candidates(s, 8)
    #
    # mask_token = tokenizer.mask_token
    result = dict()
    for entity in tqdm(candidates):
        for label in labels:
            # 根据prompt生成句子
            sentence = add_prompt(s, entity, label)
            token_ids = tokenizer.encode(sentence)
            prompt_starts_at = token_ids.index(tokenizer.sep_token_id) + 1
            #
            inputs = tokenizer(sentence,
                               padding=True, max_length=max_length,
                               truncation=True, return_tensors="pt")
            outputs = model(**inputs)
            outputs = softmax(outputs[0])
            # print(outputs)
            probability = 1
            for i in range(prompt_starts_at, len(token_ids) - 1):
                _id = token_ids[i]
                probability *= outputs[0][i][_id].item()
            #
            if entity not in result:
                result[entity] = []
            result[entity].append((label, probability))
    # 组内排序，只留其一
    for key in result:
        sorted_labels = sorted(result[key], key=lambda x: x[1])
        result[key] = sorted_labels[-1]
    # 组间排序，保留nbest
    best = sorted(result.items(), key=lambda x: x[1][1], reverse=True)[0:n_best]
    best = [(item[0], item[1][0]) for item in best]
    print(json.dumps(result, ensure_ascii=False))
    return {"sentence": s, "best": best}


if __name__ == "__main__":
    mode = "test"
    if "train" == mode:
        # 1) train
        train()
    else:
        # 2) predict
        state_dict = torch.load("best_model.pt")
        model.load_state_dict(state_dict)
        data = load_data()
        with open("result.txt", "w", encoding="utf8")as fo:
            i = 0
            length = len(data)
            for d in data:
                i += 1
                print("Progress: {}/{}".format(i, length))
                sys.stdout.flush()
                #
                rlt = predict(d)
                fo.write(json.dumps(rlt, ensure_ascii=False)+"\n")
                fo.flush()
    print("Done.")
