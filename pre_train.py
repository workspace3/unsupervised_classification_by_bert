#! -*- coding:utf-8 -*-
# 美肤助手分类，利用MLM做 Zero-Shot/Few-Shot/Semi-Supervised Learning
# 在https://github.com/bojone/Pattern-Exploiting-Training基础上做了符合自己数据的改动

import os
import numpy as np
from bert4keras.backend import keras, K
from bert4keras.layers import Loss
from bert4keras.tokenizers import Tokenizer
from bert4keras.models import build_transformer_model
from bert4keras.optimizers import Adam
from bert4keras.snippets import sequence_padding, DataGenerator
# from bert4keras.snippets import open
from keras.layers import Lambda, Dense

os.environ["CUDA_VISIBLE_DEVICES"] = ""

# label_mapping = {
#     u"__label__询问产品": u"产品",
#     u"__label__护肤步骤": u"步骤",
#     u"__label__肌肤问题": u"肌肤",
#     u"__label__询问安全": u"安全",
#     # "__label__询问包含某成分的产品": "成分产品",
#     u"__label__询问产品的成分": u"成分",
#     u"__label__询问功效": u"功效",
#     u"__label__询问口碑": u"口碑",
#     # u"__label__询问状态（拍照测肤）": u"状态"
# }

# labels = [
#     u'文化', u'娱乐', u'体育', u'财经', u'房产', u'汽车', u'教育', u'科技', u'军事', u'旅游', u'国际',
#     u'证券', u'农业', u'电竞', u'民生'
# ]
# labels = list(label_mapping.values())
# num_classes = len(labels)
maxlen = 128
batch_size = 32
config_path = '../bert/chinese_L-12_H-768_A-12/bert_config.json'
checkpoint_path = '../bert/chinese_L-12_H-768_A-12/bert_model.ckpt'
dict_path = '../bert/chinese_L-12_H-768_A-12/vocab.txt'
domain_pretrained_model_path = 'domain_pretrained_model.weights'


def load_data(filename):
    D = []
    with open(filename, encoding='utf-8') as fi:
        for line in fi.readlines():
            line = line.strip()
            if len(line) > 0:
                D.append(line)
    return D


# 加载数据集，只截取一部分，模拟小数据集
train_data = load_data('./data/beauty_domain/domain_corpus.txt')

# 建立分词器
tokenizer = Tokenizer(dict_path, do_lower_case=True)

# 对应的任务描述
prefix = u'关于产品。'
mask_idxs = [3, 4]


def random_masking(token_ids):
    """对输入进行随机mask
    """
    rands = np.random.random(len(token_ids))
    source, target = [], []
    for r, t in zip(rands, token_ids):
        if r < 0.15 * 0.8:
            source.append(tokenizer._token_mask_id)
            target.append(t)
        elif r < 0.15 * 0.9:
            source.append(t)
            target.append(t)
        elif r < 0.15:
            source.append(np.random.choice(tokenizer._vocab_size - 1) + 1)
            target.append(t)
        else:
            source.append(t)
            target.append(0)
    return source, target


class data_generator(DataGenerator):
    """数据生成器
    """
    def __iter__(self, random=False):
        batch_token_ids, batch_segment_ids, batch_output_ids = [], [], []
        for is_end, text in self.sample(random):
            token_ids, segment_ids = tokenizer.encode(text, maxlen=maxlen)
            if random:
                source_ids, target_ids = random_masking(token_ids)
            else:
                source_ids, target_ids = token_ids[:], token_ids[:]
            batch_token_ids.append(source_ids)
            batch_segment_ids.append(segment_ids)
            batch_output_ids.append(target_ids)
            if len(batch_token_ids) == self.batch_size or is_end:
                batch_token_ids = sequence_padding(batch_token_ids)
                batch_segment_ids = sequence_padding(batch_segment_ids)
                batch_output_ids = sequence_padding(batch_output_ids)
                yield [
                    batch_token_ids, batch_segment_ids, batch_output_ids
                ], None
                batch_token_ids, batch_segment_ids, batch_output_ids = [], [], []


class CrossEntropy(Loss):
    """交叉熵作为loss，并mask掉输入部分
    """
    def compute_loss(self, inputs, mask=None):
        y_true, y_pred = inputs
        y_mask = K.cast(K.not_equal(y_true, 0), K.floatx())
        accuracy = keras.metrics.sparse_categorical_accuracy(y_true, y_pred)
        accuracy = K.sum(accuracy * y_mask) / K.sum(y_mask)
        self.add_metric(accuracy, name='accuracy')
        loss = K.sparse_categorical_crossentropy(y_true, y_pred)
        loss = K.sum(loss * y_mask) / K.sum(y_mask)
        return loss


# 加载预训练模型
model = build_transformer_model(
    config_path=config_path, checkpoint_path=checkpoint_path, with_mlm=True
)
# 如果有领域预训练模型，那么直接加载领域预训练模型的参数
if os.path.exists(domain_pretrained_model_path):
    model.load_weights(domain_pretrained_model_path)

# 训练用模型
y_in = keras.layers.Input(shape=(None,))
outputs = CrossEntropy(1)([y_in, model.output])

train_model = keras.models.Model(model.inputs + [y_in], outputs)
train_model.compile(optimizer=Adam(1e-5))
train_model.summary()

# 转换数据集
train_generator = data_generator(train_data, batch_size)


class Evaluator(keras.callbacks.Callback):
    def __init__(self):
        self.best_acc = 0.0

    def on_epoch_end(self, epoch, logs=None):
        # model.save_weights('domain_mlm_model.weights')
        print("accuracy in logs: {:.5f}".format(logs.get("accuracy")))
        if logs.get("accuracy") > self.best_acc:
            self.best_acc = logs.get("accuracy")
            model.save_weights(domain_pretrained_model_path)
            print("Found better model at epoch {}. Saved to '{}'".format(
                epoch, domain_pretrained_model_path))


if __name__ == '__main__':
    evaluator = Evaluator()
    #
    train_model.fit_generator(
        train_generator.forfit(),
        steps_per_epoch=len(train_generator),
        # steps_per_epoch=3,
        epochs=3,
        callbacks=[evaluator]
    )
